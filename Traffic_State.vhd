-- Group 14, Shrey Varma & Tyler Chen
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY Traffic_State IS PORT (
    clk_input, reset : IN STD_LOGIC; -- sm_clock from the clock generator
    NS_req, EW_req, blink : IN STD_LOGIC; -- NS and EW ped crossing requests from the push-buttons
    NS_req_clear, EW_req_clear : OUT STD_LOGIC; -- NS and EW ped crossing requests clear signals
    NS_crossing, EW_crossing : OUT STD_LOGIC; -- NS and EW ped crossing signals
    state : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); -- state of the traffic light controller
    hex_NS, hex_EW : OUT STD_LOGIC_VECTOR(6 DOWNTO 0) -- hex values for the 7-segment display
);
END ENTITY;

ARCHITECTURE SM OF Traffic_State IS

    TYPE STATE_NAMES IS (S0, S1, S2, S3, S4, S5, S6, S7, S8, S9, S10, S11, S12, S13, S14, S15); -- list all the STATE_NAMES values
    SIGNAL current_state, next_state : STATE_NAMES; -- signals of type STATE_NAMES
    SIGNAL NS, EW : STD_LOGIC_VECTOR(1 DOWNTO 0); -- internal signals for the traffic light colors

BEGIN
    -------------------------------------------------------------------------------
    --State Machine:
    -------------------------------------------------------------------------------

    Register_Section : PROCESS (clk_input) -- this process updates with a clock
    BEGIN
        IF (rising_edge(clk_input)) THEN
            IF (reset = '1') THEN
                current_state <= S0; -- reset to initial state
            ELSIF (reset = '0') THEN
                current_state <= next_State; -- update in Transition_Section
            END IF;
        END IF;
    END PROCESS;

    Transition_Section : PROCESS (current_state, EW_req, NS_req)

    BEGIN
        NS_req_clear <= '0'; -- initialize NS_req_clear to 0
        EW_req_clear <= '0'; -- initialize EW_req_clear to 0
        CASE current_state IS
            WHEN S0 =>
                IF ((NS_req = '0') AND (EW_req = '1')) THEN
                    next_state <= S6; -- jump to state S6 if NS_req is 0 and EW_req is 1
                ELSE
                    next_state <= S1; -- jump to state S1 otherwise
                END IF;
                -- similar logic for S1
            WHEN S1 =>
                IF ((NS_req = '0') AND (EW_req = '1')) THEN
                    next_state <= S6;
                ELSE
                    next_state <= S2;
                END IF;

                -- transition to the next state
            WHEN S2 =>
                next_state <= S3;

            WHEN S3 =>
                next_state <= S4;

            WHEN S4 =>
                next_state <= S5;

            WHEN S5 =>
                next_state <= S6;

            WHEN S6 =>
                next_state <= S7;
                NS_req_clear <= '1'; -- clear NS crossing request

            WHEN S7 =>
                next_state <= S8;

            WHEN S8 =>
                IF ((NS_req = '1') AND (EW_req = '0')) THEN
                    next_state <= S14; -- jump to state S14 if NS_req is 1 and EW_req is 0
                ELSE
                    next_state <= S9; -- jump to state S9 otherwise
                END IF;
                -- similar logic for S9
            WHEN S9 =>
                IF ((NS_req = '1') AND (EW_req = '0')) THEN
                    next_state <= S14;
                ELSE
                    next_state <= S10;
                END IF;
                -- transition to the next state
            WHEN S10 =>
                next_state <= S11;

            WHEN S11 =>
                next_state <= S12;

            WHEN S12 =>
                next_state <= S13;

            WHEN S13 =>
                next_state <= S14;

            WHEN S14 =>
                next_state <= S15;
                EW_req_clear <= '1'; -- clear EW crossing request

            WHEN S15 =>
                next_state <= S0;

        END CASE;
    END PROCESS;

    Decoder_Section : PROCESS (current_state)

    BEGIN
        NS_crossing <= '0'; -- initialize NS crossing signal to 0
        EW_crossing <= '0'; -- initialize EW crossing signal to 0

        -- 00 = blinking green, 01 = green, 10 = amber, 11 = red
        CASE current_state IS
            WHEN S0 | S1 =>
                NS <= "00"; -- blinking green
                EW <= "11"; -- red

            WHEN S2 | S3 | S4 | S5 =>
                NS <= "01"; -- green
                NS_crossing <= '1'; -- NS crossing signal active
                EW <= "11"; -- red

            WHEN S6 | S7 =>
                NS <= "10"; -- amber
                EW <= "11"; -- red

            WHEN S8 | S9 =>
                NS <= "11"; -- red
                EW <= "00"; -- blinking green

            WHEN S10 | S11 | S12 | S13 =>
                NS <= "11"; -- red
                EW <= "01"; -- green
                EW_crossing <= '1'; -- EW crossing signal active

            WHEN S14 | S15 =>
                NS <= "11"; -- red
                EW <= "10"; -- amber

        END CASE;

        CASE current_state IS -- update the state signal to the appropriate value
            WHEN S0 =>
                state <= "0000";
            WHEN S1 >=
                state <= "0001";
            WHEN S2 =>
                state <= "0010";
            WHEN S3 =>
                state <= "0011";
            WHEN S4 =>
                state <= "0100";
            WHEN S5 =>
                state <= "0101";
            WHEN S6 =>
                state <= "0110";
            WHEN S7 =>
                state <= "0111";
            WHEN S8 =>
                state <= "1000";
            WHEN S9 =>
                state <= "1001";
            WHEN S10 =>
                state <= "1010";
            WHEN S11 =>
                state <= "1011";
            WHEN S12 =>
                state <= "1100";
            WHEN S13 =>
                state <= "1101";
            WHEN S14 =>
                state <= "1110";
            WHEN S15 =>
                state <= "1111";
        END CASE;
    END PROCESS;
    -- seven-segment display hex values
    -- segments: g f e d c b a
    -- turn on a for red, g for amber, d for green
    PROCESS (NS) BEGIN
        CASE NS IS
            WHEN "00" =>
                IF (blink = '1') THEN -- activate when blink signal is high
                    hex_NS <= "0001000"; -- turn d segment on
                ELSE
                    hex_NS <= "0000000"; -- turn all segments off
                END IF;
            WHEN "01" =>
                hex_NS <= "0001000"; -- turn d segment on
            WHEN "10" =>
                hex_NS <= "1000000"; -- turn g segment on
            WHEN "11" =>
                hex_NS <= "0000001"; -- turn a segment on
        END CASE;
    END PROCESS;
 -- repeat the same process for EW
    PROCESS (EW) BEGIN
        CASE EW IS
            WHEN "00" =>
                IF (blink = '1') THEN
                    hex_EW <= "0001000";
                ELSE
                    hex_EW <= "0000000";
                END IF;
            WHEN "01" =>
                hex_EW <= "0001000";
            WHEN "10" =>
                hex_EW <= "1000000";
            WHEN "11" =>
                hex_EW <= "0000001";
        END CASE;
    END PROCESS;

END ARCHITECTURE SM;