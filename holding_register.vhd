-- Group 14, Shrey Varma & Tyler Chen
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
ENTITY holding_register IS PORT (

    clk : IN STD_LOGIC; -- 50 MHz clock
    reset : IN STD_LOGIC; -- reset signal
    register_clr : IN STD_LOGIC; -- clear signal
    din : IN STD_LOGIC; -- data input
    dout : OUT STD_LOGIC -- data output
);
END holding_register;

ARCHITECTURE circuit OF holding_register IS

    SIGNAL sreg : STD_LOGIC; -- output from d flip flop
    SIGNAL clear, input : STD_LOGIC; -- temporary signals to hold values after gates
BEGIN

    PROCESS (clk) BEGIN
        IF (rising_edge(clk)) THEN -- d flip flop is triggered on rising edge of clock
            clear <= register_clr OR reset;
            input <= sreg OR din;
            sreg <= ((NOT clear) AND input);
            dout <= sreg;
        END IF;
    END PROCESS;
END;