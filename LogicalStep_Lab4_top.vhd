-- Group 14, Shrey Varma & Tyler Chen 
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY LogicalStep_Lab4_top IS
	PORT (
		clkin_50 : IN STD_LOGIC; -- The 50 MHz FPGA Clockinput
		rst_n : IN STD_LOGIC; -- The RESET input (ACTIVE LOW)
		pb_n : IN STD_LOGIC_VECTOR(3 DOWNTO 0); -- The push-button inputs (ACTIVE LOW)
		sw : IN STD_LOGIC_VECTOR(7 DOWNTO 0); -- The switch inputs
		leds : OUT STD_LOGIC_VECTOR(7 DOWNTO 0); -- for displaying the the lab4 project details
		-------------------------------------------------------------
		-- you can add temporary output ports here if you need to debug your design 
		-- or to add internal signals for your simulations
		-------------------------------------------------------------

		seg7_data : OUT STD_LOGIC_VECTOR(6 DOWNTO 0); -- 7-bit outputs to a 7-segment
		seg7_char1 : OUT STD_LOGIC; -- seg7 digi selectors
		seg7_char2 : OUT STD_LOGIC -- seg7 digi selectors
	);
END LogicalStep_Lab4_top;

ARCHITECTURE SimpleCircuit OF LogicalStep_Lab4_top IS
	COMPONENT segment7_mux PORT (
		clk : IN STD_LOGIC := '0';
		DIN2 : IN STD_LOGIC_VECTOR(6 DOWNTO 0); --bits 6 to 0 represent segments G,F,E,D,C,B,A
		DIN1 : IN STD_LOGIC_VECTOR(6 DOWNTO 0); --bits 6 to 0 represent segments G,F,E,D,C,B,A
		DOUT : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
		DIG2 : OUT STD_LOGIC;
		DIG1 : OUT STD_LOGIC
		);
	END COMPONENT;

	COMPONENT Traffic_State PORT (
		clk_input, reset : IN STD_LOGIC; -- sm_clock from the clock generator
		NS_req, EW_req, blink : IN STD_LOGIC; -- NS and EW ped crossing requests from the push-buttons
		NS_req_clear, EW_req_clear : OUT STD_LOGIC; -- NS and EW ped crossing requests clear signals
		NS_crossing, EW_crossing : OUT STD_LOGIC; -- NS and EW ped crossing signals
		state : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); -- state of the traffic light controller
		hex_NS, hex_EW : OUT STD_LOGIC_VECTOR(6 DOWNTO 0) -- hex values for the 7-segment display
		);
	END COMPONENT;

	COMPONENT holding_register PORT (
		clk : IN STD_LOGIC; -- 50 MHz clock
		reset : IN STD_LOGIC; -- reset signal
		register_clr : IN STD_LOGIC; -- clear signal
		din : IN STD_LOGIC; -- data input
		dout : OUT STD_LOGIC -- data output
		);
	END COMPONENT;

	COMPONENT clock_generator PORT (
		sim_mode : IN BOOLEAN; -- true for simulation, false for download to the board
		reset : IN STD_LOGIC; -- reset signal
		clkin : IN STD_LOGIC; -- 50 MHz clock input
		sm_clken : OUT STD_LOGIC; -- state machine clock 
		blink : OUT STD_LOGIC -- blink clock signal
		);
	END COMPONENT;

	COMPONENT PB_filters PORT (
		clkin : IN STD_LOGIC;
		rst_n : IN STD_LOGIC;
		rst_n_filtered : OUT STD_LOGIC;
		pb_n : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
		pb_n_filtered : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
		);
	END COMPONENT;

	COMPONENT PB_inverters PORT (
		rst_n : IN STD_LOGIC; -- active low reset
		rst : OUT STD_LOGIC; -- active high reset							 
		pb_n_filtered : IN STD_LOGIC_VECTOR (3 DOWNTO 0); -- active low push-button inputs
		pb : OUT STD_LOGIC_VECTOR(3 DOWNTO 0) -- active high push-button inputs							 
		);
	END COMPONENT;

	COMPONENT synchronizer PORT (-- synchronizes data input with the 50 MHz clock
		clk : IN STD_LOGIC; -- 50 MHz clock
		reset : IN STD_LOGIC; -- reset signal
		din : IN STD_LOGIC; -- data input
		dout : OUT STD_LOGIC -- data output
		);
	END COMPONENT;

	----------------------------------------------------------------------------------------------------
	CONSTANT sim_mode : BOOLEAN := FALSE; -- set to FALSE for LogicalStep board downloads and TRUE for SIMULATIONS
	SIGNAL rst, rst_n_filtered, synch_rst : STD_LOGIC; -- reset signals
	SIGNAL sm_clken, blink_sig : STD_LOGIC; -- state machine clock and blink signals
	SIGNAL pb_n_filtered, pb : STD_LOGIC_VECTOR(3 DOWNTO 0); -- push-button inputs
	SIGNAL NS_req, EW_req : STD_LOGIC; -- NS and EW pedestrian crossing requests
	SIGNAL NS_req_hold, EW_req_hold : STD_LOGIC; -- NS and EW pedestrian crossing requests hold signals
	SIGNAL NS_req_clear, EW_req_clear : STD_LOGIC; -- NS and EW pedestrian crossing requests clear signals
	SIGNAL hex_NS, hex_EW : STD_LOGIC_VECTOR(6 DOWNTO 0); -- hex values for the 7-segment display

BEGIN
	----------------------------------------------------------------------------------------------------
	INST0 : pb_filters PORT MAP(clkin_50, rst_n, rst_n_filtered, pb_n, pb_n_filtered); -- filter the push-button inputs
	INST1 : pb_inverters PORT MAP(rst_n_filtered, rst, pb_n_filtered, pb); -- invert the push-button inputs
	INST2 : synchronizer PORT MAP(clkin_50, synch_rst, rst, synch_rst); -- the synchronizer is also reset by synch_rst.
	INST3 : clock_generator PORT MAP(sim_mode, synch_rst, clkin_50, sm_clken, blink_sig); -- generate the state machine clock and blink signals

	INST4 : synchronizer PORT MAP(clkin_50, synch_rst, pb(0), NS_req); -- synchronize the NS pedestrian crossing request
	INST5 : synchronizer PORT MAP(clkin_50, synch_rst, pb(1), EW_req); -- synchronize the EW pedestrian crossing request
	INST6 : holding_register PORT MAP(clkin_50, synch_rst, NS_req_clear, NS_req, NS_req_hold); -- hold the NS pedestrian crossing request
	INST7 : holding_register PORT MAP(clkin_50, synch_rst, EW_req_clear, EW_req, EW_req_hold); -- hold the EW pedestrian crossing request

	leds(3) <= EW_req_hold; -- display the EW pedestrian crossing request on LED3
	leds(1) <= NS_req_hold; -- display the NS pedestrian crossing request on LED1

	INST8 : Traffic_State PORT MAP(sm_clken, synch_rst, NS_req_hold, EW_req_hold, blink_sig, NS_req_clear, EW_req_clear, leds(0), leds(2), leds(7 DOWNTO 4), hex_NS, hex_EW); -- the traffic state machine
	INST9 : segment7_mux PORT MAP(clkin_50, hex_NS, hex_EW, seg7_data, seg7_char2, seg7_char1); -- the 7-segment display
END SimpleCircuit;