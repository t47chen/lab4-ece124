-- Group 14, Shrey Varma & Tyler Chen
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
ENTITY synchronizer IS PORT (

    clk : IN STD_LOGIC; -- 50 MHz clock
    reset : IN STD_LOGIC; -- reset signal
    din : IN STD_LOGIC; -- data input
    dout : OUT STD_LOGIC -- data output
);
END synchronizer;
ARCHITECTURE circuit OF synchronizer IS

    SIGNAL sreg : STD_LOGIC_VECTOR(1 DOWNTO 0); -- holds output of 2 d flip flops
BEGIN

    PROCESS (clk) BEGIN
        IF (rising_edge(clk)) THEN -- flip flops are triggered on rising edge of clock
            IF (reset = '1') THEN
                sreg <= "00"; -- reset the flip flops
                dout <= '0'; -- output is 0
            ELSE
                dout <= sreg(1); -- dout is output of 2nd flip flop
                sreg(1) <= sreg(0); -- 2nd flip flop input is output of 1st flip flop
                sreg(0) <= din; -- 1st flip flop input is din
            END IF;
        END IF;

    END PROCESS;
END;